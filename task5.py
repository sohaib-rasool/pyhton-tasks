import copy
import matplotlib.pyplot as plt

def square(list):
    LIST=copy.deepcopy(list)
    for i in range(0,len(LIST)):
        LIST[i]=LIST[i]**2
    return LIST


def cube(list):
    LIST=copy.deepcopy(list)
    for i in range(0,len(LIST)):
         LIST[i]=LIST[i]**3
    return LIST    


    
X= [1, 2, 3 , 4, 5, 6, 7, 8, 9, 10]
print(square(X))
print(cube(X))



plt.figure("Square")
plt.plot(X, square(X))
plt.xlabel('Number')
plt.ylabel('Squared Number')
#plt.show()
plt.figure("Cube")
plt.plot(X, cube(X))
plt.xlabel('Number')
plt.ylabel('Cubed Number')
plt.show()
