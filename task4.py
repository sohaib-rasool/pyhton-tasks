import random 
import argparse
import sys
import json



# Create the parser
my_parser = argparse.ArgumentParser(description='Monte Carlo\'s estimation of pi')
my_group = my_parser.add_mutually_exclusive_group(required=True)
# Add the arguments
my_group.add_argument('-i',
                       action='store',
                       nargs=1,
                       type=int,
                       metavar='Iteration',
                       help='Number of Iterations')
my_group.add_argument('-j',
                       action='store_true',
                       
                       help='This flag will use iterations from a JSON file in the same directory')                       

# Execute parse_args()
args = my_parser.parse_args()


if args.j: 

    with open("task4.json", "r") as read_file:
        INTERVAL = json.load(read_file)
    print("Using",INTERVAL,"for # of iterations as in task4.json file")
    #print("-j")
    #sys.exit()
else:
    
    #try:
    #   print("Iterations: ",args.i[0], "\n")
    #except :
    #    print("Please use flags according to the help.")
    #    sys.exit()

    INTERVAL= args.i[0]

circle_points= 0
square_points= 0

# Total Random numbers generated= possible x 
# values* possible y values 
for i in range(0,INTERVAL): 

	# Randomly generated x and y values from a 
	# uniform distribution 
	# Range of x and y values is -1 to 1 
	rand_x= random.uniform(-1, 1) 
	rand_y= random.uniform(-1, 1) 

	# Distance between (x, y) from the origin 
	origin_dist= rand_x**2 + rand_y**2

	# Checking if (x, y) lies inside the circle 
	if origin_dist<= 1: 
		circle_points+= 1

	square_points+= 1

	# Estimating value of pi, 
	# pi= 4*(no. of points generated inside the 
	# circle)/ (no. of points generated inside the square) 
	pi = 4* circle_points/ square_points 

## print(rand_x, rand_y, circle_points, square_points, "-", pi) 
## print("\n") 

print("Final Estimation of Pi=", pi)	 
